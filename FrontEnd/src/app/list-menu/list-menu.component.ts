import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Menu} from '../../model/Menu';
import {MenuService} from '../../service/service-menu/menu.service';
import {MenuDialogComponent} from "../dialog/menu-dialog/menu-dialog.component";
import {MatDialog} from "@angular/material";
import {MenuDeleteDialogComponent} from "../dialog/menu-delete-dialog/menu-delete-dialog.component";

@Component({
  selector: 'app-list-menu',
  templateUrl: './list-menu.component.html',
  styleUrls: ['./list-menu.component.css']
})

export class ListMenuComponent implements OnInit {

  private _menus: Menu[] = [];

  constructor( public dialog: MatDialog, private menuService: MenuService) {

  }

  ngOnInit() {
    this.menuService.getAllMenu().subscribe(allMenu => this.menus = Menu.fromArrayJson(allMenu));
  }

  public openDeleteDialog(i: number) {
    const dialogRef = this.dialog.open(MenuDeleteDialogComponent, {
      data : {
        menus: this._menus,
        index : i
      }
    });
  }

  get menus(): Menu[] {
    return this._menus;
  }

  set menus(value: Menu[]) {
    this._menus = value;
  }

  public openAddDialog() {
    const dialogRef = this.dialog.open(MenuDialogComponent, {
      data : {
        operation: true,
        menu: new Menu('', 0, '', '', 0),
        menus : this.menus
      }
    });
  }

  public openUpdateDialog(index: number) {
    const dialogRef = this.dialog.open(MenuDialogComponent, {
      data : {
        operation: false,
        menu : this._menus[index],
        menus : this.menus
      }
    });
  }
}
