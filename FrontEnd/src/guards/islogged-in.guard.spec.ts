import { TestBed, async, inject } from '@angular/core/testing';

import { IsloggedInGuard } from './islogged-in.guard';

describe('IsloggedInGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsloggedInGuard]
    });
  });

  it('should ...', inject([IsloggedInGuard], (guard: IsloggedInGuard) => {
    expect(guard).toBeTruthy();
  }));
});
