SET IDENTITY_INSERT [dbo].[Employee_Type] ON
INSERT INTO [dbo].[Employee_Type] ([idEmployeeType], [nameEmployeeType]) VALUES (1, N'Patron')
INSERT INTO [dbo].[Employee_Type] ([idEmployeeType], [nameEmployeeType]) VALUES (2, N'Cuisinier')
SET IDENTITY_INSERT [dbo].[Employee_Type] OFF
