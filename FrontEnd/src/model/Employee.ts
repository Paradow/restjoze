export class Employee {
  private _employeeID: number;
  private _lastNameEmployee: string;
  private _firstNameEmployee: string;
  private _phoneNumber: string;
  private _mailAdress: string;
  private _idEmployeeType: number;
  private _street: string;
  private _num: number;
  private _postalCode: number;
  private _city: string;
  private _username: string;
  private _password: string;


  constructor(lastNameEmployee: string, firstNameEmployee: string, phoneNumber: string, mailAdress: string, idEmployeeType: number, street: string, num: number, postalCode: number, city: string, username: string, password: string) {

    this._lastNameEmployee = lastNameEmployee;
    this._firstNameEmployee = firstNameEmployee;
    this._phoneNumber = phoneNumber;
    this._mailAdress = mailAdress;
    this._idEmployeeType = idEmployeeType;
    this._street = street;
    this._num = num;
    this._postalCode = postalCode;
    this._city = city;
    this._username = username;
    this._password = password;
  }

  public static fromJson(rawEmployee: any): Employee {
    const idEmployee = rawEmployee['IdEmployee'];
    const lastNameEmployee = rawEmployee['LastNameEmployee'];
    const firstNameEmployee = rawEmployee['FirstNameEmployee'];
    const phoneNumber = rawEmployee['PhoneNumber'];
    const mailAdress = rawEmployee['MailAdress'];
    const idEmployeeType = rawEmployee['IdEmployeeType'];
    const street = rawEmployee['Street'];
    const num = rawEmployee['Num'];
    const postalCode = rawEmployee['PostalCode'];
    const city = rawEmployee['City'];
    const username = rawEmployee['Username'];
    const password = rawEmployee['Password'];
    const employee = new Employee(
      lastNameEmployee,
      firstNameEmployee,
      phoneNumber,
      mailAdress,
      idEmployeeType,
      street,
      num,
      postalCode,
      city,
      username,
      password
    );
    employee._employeeID = idEmployee;


    return employee;
  }

  public static fromArrayJson(rawsEmployee: any[]): Employee[] {
    return rawsEmployee.map(Employee.fromJson);
  }

  public getCleanDataForSending() {
    return {
      idEmployee: this._employeeID,
      lastNameEmployee: this._lastNameEmployee,
      firstNameEmployee: this._firstNameEmployee,
      phoneNumber: this._phoneNumber,
      mailAdress: this._mailAdress,
      idEmployeeType: this._idEmployeeType,
      street: this._street,
      num: this._num,
      postalCode: this._postalCode,
      city: this._city,
      username: this._username,
      password: this._password

    }
  }

  public clone(): Employee {
    const e = new Employee(
      this._lastNameEmployee,
      this._firstNameEmployee,
      this._phoneNumber,
      this._mailAdress,
      this._idEmployeeType,
      this._street,
      this._num,
      this._postalCode,
      this._city,
      this._username,
      this._password)
    e._employeeID = this._employeeID;
    return e;
  }


  public get employeeID(): number {
    return this._employeeID;
  }

  public set employeeID(value: number) {
    this._employeeID = value;
  }

  public get lastNameEmployee(): string {
    return this._lastNameEmployee;
  }

  public set lastNameEmployee(value: string) {
    this._lastNameEmployee = value;
  }

  public get firstNameEmployee(): string {
    return this._firstNameEmployee;
  }

  public set firstNameEmployee(value: string) {
    this._firstNameEmployee = value;
  }

  public get phoneNumber(): string {
    return this._phoneNumber;
  }

  public set phoneNumber(value: string) {
    this._phoneNumber = value;
  }

  public get mailAdress(): string {
    return this._mailAdress;
  }

  public set mailAdress(value: string) {
    this._mailAdress = value;
  }

  public get idEmployeeType(): number {
    return this._idEmployeeType;
  }

  public set idEmployeeType(value: number) {
    this._idEmployeeType = value;
  }

  public get street(): string {
    return this._street;
  }

  public set street(value: string) {
    this._street = value;
  }

  public get num(): number {
    return this._num;
  }

  public set num(value: number) {
    this._num = value;
  }

  public get postalCode(): number {
    return this._postalCode;
  }

  public set postalCode(value: number) {
    this._postalCode = value;
  }

  public get city(): string {
    return this._city;
  }

  public set city(value: string) {
    this._city = value;
  }

  public get username(): string {
    return this._username;
  }

  public set username(value: string) {
    this._username = value;
  }

  public get password(): string {
    return this._password;
  }

  public set password(value: string) {
    this._password = value;
  }


}
