import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule, Routes} from '@angular/router';



import {ManagementManagerComponent} from '../management-manager/management-manager.component';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';
import {ListTypeProduitComponent} from '../list-type-produit/list-type-produit.component';
import {ListTypeEmployeeComponent} from '../list-type-employee/list-type-employee.component';
import {LoginComponent} from '../login/login.component';
import {ListMenuComponent} from '../list-menu/list-menu.component';

import { PatronGuard } from '../../guards/patron.guard';
import { CuisinierGuard } from '../../guards/cuisinier.guard';
import { IsloggedInGuard } from '../../guards/islogged-in.guard';


const appRoutes: Routes = [

  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
    canActivate: [IsloggedInGuard]
  },
  {
    path: 'management',
    component: ManagementManagerComponent,
    children: [{
        path: '',
        redirectTo: 'products',
        pathMatch: 'full',
        canActivate:[PatronGuard]
      },
      {
        path: 'products',
        component: ListTypeProduitComponent,
        canActivate:[PatronGuard]
      },
      {
        path: 'employees',
        component: ListTypeEmployeeComponent,
        canActivate:[PatronGuard]
      },
      {
        path: 'menu',
        component: ListMenuComponent,
        canActivate:[CuisinierGuard]
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate:[IsloggedInGuard]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];



@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
    CommonModule
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule {
}
