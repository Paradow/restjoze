USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Menu] Script Date: 12/8/2017 15:44:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Menu] (
    [idMenu]        INT           IDENTITY (1, 1) NOT NULL,
    [nameMenu]      VARCHAR (100) NOT NULL,
    [priceMenu]     NUMERIC (5)   NOT NULL,
    [dateStartMenu] DATETIME      NOT NULL,
    [dateEndMenu]   DATETIME      NOT NULL,
    [nbPersMenu]    NUMERIC (2)   NOT NULL
);


