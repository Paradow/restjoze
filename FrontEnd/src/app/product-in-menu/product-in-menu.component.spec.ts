import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductInMenuComponent } from './product-in-menu.component';

describe('ProductInMenuComponent', () => {
  let component: ProductInMenuComponent;
  let fixture: ComponentFixture<ProductInMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductInMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductInMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
