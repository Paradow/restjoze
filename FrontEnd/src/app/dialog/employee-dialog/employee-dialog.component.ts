import {Component, OnInit, Inject, Input} from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material';
import {NgForm} from '@angular/forms/src/directives/ng_form';
import {EmployeeService} from '../../../service/service-employee/employee.service';
import {Employee} from '../../../model/Employee';

@Component({
  selector: 'app-employee-dialog',
  templateUrl: './employee-dialog.component.html',
  styleUrls: ['./employee-dialog.component.css']
})
export class EmployeeDialogComponent implements OnInit {

  private _update: boolean;

  private _employee: Employee;

  private dialogType: boolean;

  name = new FormControl('', [Validators.required]);

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public employeeService: EmployeeService) {
  }

  public onSubmit() {
    if (this.dialogType) {
      this.updateEmployee();
    } else {
      this.createEmployee();
    }

  }

  ngOnInit() {
    this._employee = this.data.employee.clone();
    this.dialogType = this.data.inputType;
    this._update = !this.data.inputType;

  }

  public updateEmployee() {
    this.employeeService.updateEmployee(this.employee).subscribe();
    this.data.employee = this._employee.clone();
    // const i = this.data.dataSource._renderData._value.findIndex(emp => emp.employeeID == this.employee.employeeID)
    // this.data.dataSource._renderData._value[i] = this.employee;
  }

  public createEmployee() {

    this._employee.idEmployeeType = this.data.employeeType.idEmployeeType;
    this.employeeService.createEmployee(this._employee).subscribe(
      pfromdb => this._employee.employeeID = Employee.fromJson(pfromdb).employeeID
    );
    this.data.employeeType.employees.push(this._employee);
    this.data.dataSource._renderData.next(this.data.employeeType.employees);

  }

  getErrorMessage(typeInput: string) {
    return 'vous devez entrer un ' + typeInput;
  }

  get update(): boolean {
    return this._update;
  }

  set update(value: boolean) {
    this._update = value;
  }

  public get employee(): Employee {
    return this._employee;
  }

  public set employee(value: Employee) {
    this._employee = value;
  }
}
