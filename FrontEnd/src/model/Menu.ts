import {Product} from './Product';

export class Menu {
  public idMenu: number;
  public nameMenu: string;
  public priceMenu: number;
  public dateStartMenu: string;
  public dateEndMenu: string;
  public nbPersMenu: number;
  public product: Product[] = [];

  constructor(nameMenu: string, priceMenu: number, dateStartMenu: string, dateEndMenu: string, nbPersMenu: number) {
    this.nameMenu = nameMenu;
    this.priceMenu = priceMenu;
    this.dateStartMenu = dateStartMenu;
    this.dateEndMenu = dateEndMenu;
    this.nbPersMenu = nbPersMenu;
  }

  public static fromJson(rawMenu: any): Menu {
    const tmpPs = Product.fromArrayJson(rawMenu['Product']);
    const id = rawMenu['IdMenu'];
    const tmpMenu = new Menu(
      rawMenu['NameMenu'],
      rawMenu['priceMenu'],
      rawMenu['DateStartMenu'],
      rawMenu['DateEndMenu'],
      rawMenu['NbPersMenu']);
    tmpMenu.product = tmpPs;
    tmpMenu.idMenu = id;
    return tmpMenu;
  }

  public static fromArrayJson(rawsMenu: any[]): Menu[] {
    return rawsMenu.map(Menu.fromJson);
  }

  public clone(): Menu {
    let menu = new Menu(this.nameMenu,this.priceMenu,this.dateStartMenu,this.dateEndMenu, this.nbPersMenu);
    menu.idMenu = this.idMenu;
    for(let product of this.product){
      menu.product.push(product.clone());
    }
    return menu;
  }
}
