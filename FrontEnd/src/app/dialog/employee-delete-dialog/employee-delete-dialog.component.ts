import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {EmployeeService} from "../../../service/service-employee/employee.service";

@Component({
  selector: 'app-employee-delete-dialog',
  templateUrl: './employee-delete-dialog.component.html',
  styleUrls: ['./employee-delete-dialog.component.css']
})
export class EmployeeDeleteDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public employeeService: EmployeeService) {
  }

  ngOnInit() {
  }

  public deleteEmployee() {
    this.employeeService.deleteEmployee(this.data.employees[this.data.index].employeeID)
      .subscribe(
        response => {
          this.data.employees.splice(this.data.index, 1)
          this.data.dataSource._renderData.next(this.data.employees);
        },
        err => console.error(err)
      );
  }
}
