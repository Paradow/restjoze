import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTypeDeleteDialogComponent } from './product-type-delete-dialog.component';

describe('ProductTypeDeleteDialogComponent', () => {
  let component: ProductTypeDeleteDialogComponent;
  let fixture: ComponentFixture<ProductTypeDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTypeDeleteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTypeDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
