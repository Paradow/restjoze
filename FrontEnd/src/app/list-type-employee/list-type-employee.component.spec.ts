import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTypeEmployeeComponent } from './list-type-employee.component';

describe('ListTypeEmployeeComponent', () => {
  let component: ListTypeEmployeeComponent;
  let fixture: ComponentFixture<ListTypeEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTypeEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTypeEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
