USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Product] Script Date: 12/8/2017 15:46:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Product] (
    [IdProd]        INT           IDENTITY (1, 1) NOT NULL,
    [nameProd]      VARCHAR (100) NOT NULL,
    [priceProd]     NUMERIC (5)   NOT NULL,
    [stockProd]     NUMERIC (5)   NOT NULL,
    [idProductType] INT           NOT NULL,
    [urlImage]      VARCHAR (MAX) NULL
);


