import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {ProductService} from "../../../service/service-product/product.service";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-product-delete-dialog',
  templateUrl: './product-delete-dialog.component.html',
  styleUrls: ['./product-delete-dialog.component.css']
})
export class ProductDeleteDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public productService: ProductService) {
  }

  ngOnInit() {
  }

  public deleteProduct() {
    this.productService.deleteProduct(this.data.products[this.data.index].idProd)
      .subscribe(
        response => {
          this.data.products.splice(this.data.index, 1),
          this.data.dataSource._renderData.next(this.data.products);
          },
            err => console.error(err)
      );
  }
}
