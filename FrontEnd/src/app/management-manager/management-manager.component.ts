import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../service/login/login.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-management-manager',
  templateUrl: './management-manager.component.html',
  styleUrls: ['./management-manager.component.css']
})
export class ManagementManagerComponent implements OnInit {

  public tabs= [];



  constructor(private router:Router,private loginService:LoginService, private snackBar:MatSnackBar) {
    this.tabs=[
      {path:"products", label:'Products',isActive:'true'},
      {path:"employees", label:'Employees',isActive:'false'},
      {path:"menu", label:'Menu',isaActive:'false'}
    ]
   }

  ngOnInit() {
  }
  private logOut():void{
    this.loginService.logout();
    this.snackBar.open("Good Bye !","close",{
      duration: 2000,
    })
    this.router.navigate(['/login']);
    
  }

}
