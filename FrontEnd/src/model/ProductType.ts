import { Product } from './Product';

export class ProductType {
  private _productTypeID: number;
  private _nameProductType: string;
  private _product: Product[] = [];


  constructor(nameProductType: string) {
    this.nameProductType = nameProductType;
  }

  public static fromJson(rawProductType: any): ProductType {
    const tmpPs = Product.fromArrayJson(rawProductType['Product']);
    const id = rawProductType['IdProductType'];
    const tmpPt = new ProductType(rawProductType['NameProductType']);
    tmpPt.product = tmpPs;
    tmpPt.productTypeID = id;
    return tmpPt;
  }

  public static fromArrayJson(rawsProductType: any[]): ProductType[] {
    return rawsProductType.map(ProductType.fromJson);
  }

  public getCleanDataForSending() {
    return {
      idProductType: this.productTypeID,
      nameProductType: this.nameProductType
    };
  }

  public get productTypeID(): number {
    return this._productTypeID;
  }

  public set productTypeID(value: number) {
    this._productTypeID = value;
  }

  public get nameProductType(): string {
    return this._nameProductType;
  }

  public set nameProductType(value: string) {
    this._nameProductType = value;
  }

  public get product(): Product[] {
    return this._product;
  }

  public set product(value: Product[]) {
    this._product = value;
  }

}
