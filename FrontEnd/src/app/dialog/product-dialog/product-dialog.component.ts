import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ProductService } from '../../../service/service-product/product.service';
import { Product } from '../../../model/Product';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.css']
})
export class ProductDialogComponent implements OnInit {
  private _update: Boolean = false;

  private _product: Product;

  name = new FormControl('', [Validators.required]);

  constructor( @Inject(MAT_DIALOG_DATA) public data: any, public productService: ProductService) { }

  public onSubmit() {
    this.updateProduct();
  }

  ngOnInit() {
    this.product = this.data.product.clone();
  }

  public updateProduct() {
    this.data.product = this.product.clone();
    this.productService.updateProduct(this.data.product).subscribe();
  }

  getErrorMessage(typeInput: string) {
    return 'vous devez entrer un ' + typeInput;
  }

  get update(): Boolean {
    return this._update;
  }

  set update(value: Boolean) {
    this._update = value;
  }

  public get product(): Product {
    return this._product;
  }

  public set product(value: Product) {
    this._product = value;
  }

}
