import { Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../service/login/login.service';
import { MatSnackBar } from '@angular/material';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PASSWORD_REGEX = /^([1-zA-Z0-1@.\s]{1,255})$/;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
 
  
  
})
export class LoginComponent implements OnInit {

  
  loading = false;
  error='';
  


  private username :string;
  private password :string;
  constructor(
    private router:Router,
    private loginService:LoginService,
    private snackBar:MatSnackBar
  ) { }

  ngOnInit() {
    //this.loginService.logout();
    
    //localStorage.clear();

  }
  

  login(){
    
    this.loading = true;
    this.loginService.login(this.username, this.password)
    
    .subscribe(result => {
        if (result === true) {
            // login successful
            
            
            var token = JSON.parse(localStorage.getItem("currentUser")).token;
            

            
            
            let jwtData = token.split('.')[1];
            let decodedJwtJsonData = window.atob(jwtData);
            let decodedJwtData = JSON.parse(decodedJwtJsonData);
            let role = decodedJwtData.role;
            let username = decodedJwtData.unique_name;
            this.snackBar.open("Welcome "+username,"close",{
              duration: 2000,
            });
            if(role === "Patron"){
              this.router.navigate(['/management']);
              
            }else if (role === "Cuisinier"){
              this.router.navigate(['/management/menu'])
              
            }
            
        } else {
            // login failed
            
            this.error = 'Username or password is incorrect';
            this.snackBar.open(this.error,"close",{
              duration: 2000,
            });
            
            this.loading = false;
        }
    });  }

    

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)]);

    passwordFormControl = new FormControl('', [
      Validators.required,
      Validators.pattern(PASSWORD_REGEX)]);

}

