import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DateCustomPipe implements PipeTransform {

  transform(value: string): string {
    let date: string = value.substr(8, 2) + "/" + value.substr(5, 2) + "/" + value.substr(0, 4);
    return date;
  }

}
