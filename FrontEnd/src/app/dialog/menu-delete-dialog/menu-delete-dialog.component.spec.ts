import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDeleteDialogComponent } from './menu-delete-dialog.component';

describe('MenuDeleteDialogComponent', () => {
  let component: MenuDeleteDialogComponent;
  let fixture: ComponentFixture<MenuDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDeleteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
