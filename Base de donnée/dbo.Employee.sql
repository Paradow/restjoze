USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Employee] Script Date: 12/8/2017 15:42:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee] (
    [idEmployee]        INT           IDENTITY (1, 1) NOT NULL,
    [lastNameEmployee]  VARCHAR (80)  NOT NULL,
    [firstNameEmployee] NCHAR (10)    NOT NULL,
    [phoneNumber]       VARCHAR (13)  NOT NULL,
    [mailAdress]        VARCHAR (80)  NOT NULL,
    [idEmployeeType]    INT           NOT NULL,
    [street]            VARCHAR (150) NOT NULL,
    [num]               INT           NOT NULL,
    [postalCode]        INT           NOT NULL,
    [city]              VARCHAR (80)  NOT NULL,
    [username]          VARCHAR (80)  NOT NULL,
    [password]          VARCHAR (255) NOT NULL
);


