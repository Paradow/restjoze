import { Employee } from './Employee';

export class EmployeeType {
    idEmployeeType: number;
    nameEmployeeType: string;
    employees: Employee[] = [];


    constructor(nameEmployeeType: string) {
        this.nameEmployeeType = nameEmployeeType;
    }
    public static fromJson(rawEmployeeType: any): EmployeeType {
        const tmpEmployees = Employee.fromArrayJson(rawEmployeeType['Employee']);
        const id = rawEmployeeType['IdEmployeeType'];
        const tmpEmployeetype = new EmployeeType(rawEmployeeType['NameEmployeeType'])

        tmpEmployeetype.employees = tmpEmployees;
        tmpEmployeetype.idEmployeeType = id;
        return tmpEmployeetype;
    }

    public static fromArrayJson(rawsEmployeeType: any[]): EmployeeType[] {
        return rawsEmployeeType.map(EmployeeType.fromJson);
    }
}
