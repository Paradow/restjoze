import {Component, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {EmployeeType} from '../../model/EmployeeType';
import {EmployeeTypeService} from '../../service/service-employee-type/employee-type.service';
import {EmployeeTypeAddDialogComponent} from '../dialog/employee-type-add-dialog/employee-type-add-dialog.component';
import {EmployeeTypeDeleteDialogComponent} from '../dialog/employee-type-delete-dialog/employee-type-delete-dialog.component';
import {Employee} from '../../model/Employee';
import {EmployeeDialogComponent} from '../dialog/employee-dialog/employee-dialog.component';

@Component({
  selector: 'app-list-type-employee',
  templateUrl: './list-type-employee.component.html',
  styleUrls: ['./list-type-employee.component.css']
})
export class ListTypeEmployeeComponent implements OnInit {

  private _employeeTypes: EmployeeType[] = [];
  private _displayedColumns = ['lastName', 'firstName', 'email'];


  constructor(public employeeTypeService: EmployeeTypeService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.employeeTypeService.getAllEmployeeTypes().subscribe(employeeTypes =>
      this._employeeTypes = EmployeeType.fromArrayJson(employeeTypes)
    );

  }


  public get employeeTypes(): EmployeeType[] {
    return this._employeeTypes;
  }

  public set employeeTypes(value: EmployeeType[]) {
    this._employeeTypes = value;
  }

  public openAddDialog() {
    const dialogRef = this.dialog.open(EmployeeTypeAddDialogComponent, {
      data: {
        employeeTypes: this.employeeTypes
      }
    });
  }

  public openDeleteDialog(index: number) {
    const dialogRef = this.dialog.open(EmployeeTypeDeleteDialogComponent, {
      data: {
        index: index,
        employeeTypes: this.employeeTypes,
      }
    });
  }
}
