import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unite'
})
export class UnitePipe implements PipeTransform {

  transform(value: number): any {
    if(value != 1 && value != 0) {
      return value + " persons";
    } else {
      return value + " person";
    }
  }

}
