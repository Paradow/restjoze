import {Component, Input, OnInit} from '@angular/core';
import {Menu} from '../../model/Menu';
import {MenuService} from "../../service/service-menu/menu.service";

@Component({
  selector: 'app-product-in-menu',
  templateUrl: './product-in-menu.component.html',
  styleUrls: ['./product-in-menu.component.css']
})

export class ProductInMenuComponent implements OnInit {

  constructor(private menuService: MenuService) {
  }

  @Input()
  private _operation: boolean = false;

  @Input()
  private _oMenu: Menu = new Menu('', 0, '', '', 0);

  @Input()
  private _menu: Menu;

  @Input()
  private _menus: Menu[] = [];

  ngOnInit() {
  }

  get oMenu(): Menu {
    return this._oMenu;
  }

  set oMenu(value: Menu) {
    this._oMenu = value;
  }

  get menu(): Menu {
    return this._menu;
  }

  set menu(value: Menu) {
    this._menu = value;
  }

  public deleteProduct(index: number) {
    this._menu.product.splice(index, 1)
  }

  public saveMenu() {
    if (!this._operation) {
      this.menuService.updateMenu(this.menu).subscribe();
      this.updateOriginalProduct();

    } else {
      this.menuService.createMenu(this.menu).subscribe(pfromdb => this.menu.idMenu = Menu.fromJson(pfromdb).idMenu);
      this._menus.push(this.menu);
    }
  }

  public updateOriginalProduct() {
    this.oMenu.idMenu = this.menu.idMenu;
    this.oMenu.nameMenu = this.menu.nameMenu;
    this.oMenu.priceMenu = this.menu.priceMenu;
    this.oMenu.nbPersMenu = this.menu.nbPersMenu;
    this.oMenu.dateStartMenu = this.menu.dateStartMenu;
    this.oMenu.dateEndMenu = this.menu.dateEndMenu;
    this.oMenu.product = [];
    for (let product of this.menu.product) {
      this.oMenu.product.push(product.clone());
    }
  }
}
