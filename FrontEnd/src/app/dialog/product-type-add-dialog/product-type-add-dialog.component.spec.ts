import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTypeAddDialogComponent } from './product-type-add-dialog.component';

describe('ProductTypeAddDialogComponent', () => {
  let component: ProductTypeAddDialogComponent;
  let fixture: ComponentFixture<ProductTypeAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTypeAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTypeAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
