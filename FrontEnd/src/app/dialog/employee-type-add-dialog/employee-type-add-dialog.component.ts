import {Component, Inject, OnInit} from '@angular/core';
import {ProductType} from "../../../model/ProductType";
import {MAT_DIALOG_DATA} from "@angular/material";
import { EmployeeTypeService } from '../../../service/service-employee-type/employee-type.service';
import { EmployeeType } from '../../../model/EmployeeType';

@Component({
  selector: 'app-employee-type-add-dialog',
  templateUrl: './employee-type-add-dialog.component.html',
  styleUrls: ['./employee-type-add-dialog.component.css']
})
export class EmployeeTypeAddDialogComponent implements OnInit {
 
  nameEmployeeType: string;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public  employeeTypeService: EmployeeTypeService) {
  }

  ngOnInit() {
  }

  public createTypeEmployee() {
    let et = new EmployeeType(this.nameEmployeeType);
    this.employeeTypeService.createEmployeeType(et).subscribe(etfromdb => et.idEmployeeType = EmployeeType.fromJson(etfromdb).idEmployeeType);
    this.data.employeeTypes.push(et);
  }
}
