 import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { Http, Response,Headers } from '@angular/http';
import { Employee } from '../../model/Employee';

@Injectable()
export class LoginService {
  private linkApi = 'http://restauranthelha.azurewebsites.net/api/token';

  public token: string;

     constructor(private http: Http) {
         // set token if saved in local storage
         var currentUser = JSON.parse(localStorage.getItem('currentUser'));
         this.token = currentUser && currentUser.token;
     }

     login(username: string, password: string): Observable<boolean> {
         const employee = new Employee("","","","",0,"",0,0,"",username,password);
         return this.http.post(this.linkApi, employee.getCleanDataForSending(),
         {
           headers: new Headers({
             'Content-Type': 'application/json'
           })
         })
             .map((response: Response) => {
                 // login successful if there's a jwt token in the response
                 let token = response.json();
                 if (token != "no user") {
                     // set token property
                     this.token = token;

                     // store username and jwt token in local storage to keep user logged in between page refreshes
                     localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                     // return true to indicate successful login
                     return true;
                 } else {
                     // return false to indicate failed login
                     return false;
                 }
             });
     }


     logout(): void {
         // clear token remove user from local storage to log user out
         this.token = null;
         localStorage.removeItem('currentUser');
     }

}
