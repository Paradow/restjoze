import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Menu} from '../../model/Menu';

@Injectable()
export class MenuService {

  private linkApi = 'http://restauranthelha.azurewebsites.net/api/Menus';

  constructor(private http: HttpClient) {
  }

  public getAllMenu(): Observable<any> {
    return this.http.get(this.linkApi);
  }

  public getMenu(i: number): Observable<any>{
    return this.http.get(this.linkApi +"/"+i);
  }

  public createMenu(menu: Menu): Observable<any> {
    return this.http.post(
      this.linkApi,
      JSON.stringify(menu),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  public deleteMenu(id: number): Observable<any> {
    return this.http.delete(this.linkApi, {
      params: new HttpParams().set('id', id + '')
    });
  }

  public updateMenu(menu: Menu): Observable<any> {
    return this
      .http.put(this.linkApi, JSON.stringify(menu), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        params: new HttpParams().set('id', menu.idMenu + '')
      });
  }


}
