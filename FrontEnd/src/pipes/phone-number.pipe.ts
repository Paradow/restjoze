import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phoneNumber'
})
export class PhoneNumberPipe implements PipeTransform {

  transform(value: string): string {
    let phonenumber: string = value.substr(0, 4) + "/" + value.substr(4, 2) + "." + value.substr(6, 2)+"."+value.substr(8,2);
    return phonenumber;
  }

}
