USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Bill] Script Date: 12/8/2017 15:40:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bill] (
    [idBill]          INT         IDENTITY (1, 1) NOT NULL,
    [numTable]        NUMERIC (4) NOT NULL,
    [dateTransaction] DATETIME    NOT NULL,
    [isPayed]         BIT         NOT NULL
);


