import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpModule} from '@angular/http';

// Import des animations pour angular material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule, MatNativeDateModule, MatSidenavModule, MatTabsModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatListModule} from '@angular/material/list';
import {MatSnackBarModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/'

// Components
import {ListTypeEmployeeComponent} from './list-type-employee/list-type-employee.component';
import {ManagementManagerComponent} from './management-manager/management-manager.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {TableProductComponent} from './table/table-product/table-product.component';
import {ListTypeProduitComponent} from './list-type-produit/list-type-produit.component';
import {ProductDialogComponent} from './dialog/product-dialog/product-dialog.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ProductTypeAddDialogComponent} from './dialog/product-type-add-dialog/product-type-add-dialog.component';
import {TableEmployeeComponent} from './table/table-employee/table-employee.component';
import {ProductTypeDeleteDialogComponent} from './dialog/product-type-delete-dialog/product-type-delete-dialog.component';
import {ProductDeleteDialogComponent} from './dialog/product-delete-dialog/product-delete-dialog.component';
import {ProductAddDialogComponent} from './dialog/product-add-dialog/product-add-dialog.component';
import {EmployeeTypeAddDialogComponent} from './dialog/employee-type-add-dialog/employee-type-add-dialog.component';
import {EmployeeTypeDeleteDialogComponent} from './dialog/employee-type-delete-dialog/employee-type-delete-dialog.component';
import {EmployeeDialogComponent} from './dialog/employee-dialog/employee-dialog.component';
import {EmployeeDeleteDialogComponent} from './dialog/employee-delete-dialog/employee-delete-dialog.component';
import {AddProductMenuComponent} from './add-product-menu/add-product-menu.component';
import {ProductInMenuComponent} from './product-in-menu/product-in-menu.component';
import {PhoneNumberPipe} from '../pipes/phone-number.pipe';
import {DateCustomPipe} from '../pipes/datecustom.pipe';
import {LoginComponent} from './login/login.component';
import {ListMenuComponent} from './list-menu/list-menu.component';
import {MenuDeleteDialogComponent} from "./dialog/menu-delete-dialog/menu-delete-dialog.component";

// Service
import {MenuService} from '../service/service-menu/menu.service';
import {EmployeeService} from '../service/service-employee/employee.service';
import {ProductService} from '../service/service-product/product.service';
import {ProductTypeService} from '../service/service-product-type/product-type.service';
import {EmployeeTypeService} from '../service/service-employee-type/employee-type.service';

// Module routing
import {AppRoutingModule} from './app-routing/app-routing.module';
import {LoginService} from '../service/login/login.service';
import {PatronGuard} from '../guards/patron.guard';
import {CuisinierGuard} from '../guards/cuisinier.guard';
import {IsloggedInGuard} from '../guards/islogged-in.guard';
import {MenuDialogComponent} from "./dialog/menu-dialog/menu-dialog.component";
import {UnitePipe} from '../pipes/unite.pipe';
import {AuthService} from "../service/service-auth/auth.service";
import {TokenInterceptor} from "../service/service-auth/TokenInterceptor";


@NgModule({

  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    // Import des animations pour angular material
    MatSidenavModule,
    MatMenuModule,
    MatTabsModule,
    MatToolbarModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatListModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  declarations: [
    AppComponent,
    ListTypeProduitComponent,
    ListTypeEmployeeComponent,
    ManagementManagerComponent,
    ProductDialogComponent,
    TableProductComponent,
    PageNotFoundComponent,
    TableEmployeeComponent,
    ProductTypeAddDialogComponent,
    ProductTypeDeleteDialogComponent,
    ProductDeleteDialogComponent,
    ProductAddDialogComponent,
    EmployeeTypeAddDialogComponent,
    EmployeeTypeDeleteDialogComponent,
    EmployeeDialogComponent,
    EmployeeDeleteDialogComponent,
    AddProductMenuComponent,
    ProductInMenuComponent,
    PhoneNumberPipe,
    DateCustomPipe,
    LoginComponent,
    ListMenuComponent,
    MenuDialogComponent,
    MenuDeleteDialogComponent,
    UnitePipe
  ],
  entryComponents: [
    ProductDialogComponent,
    ProductTypeAddDialogComponent,
    ProductTypeDeleteDialogComponent,
    ProductAddDialogComponent,
    ProductDeleteDialogComponent,
    EmployeeTypeAddDialogComponent,
    EmployeeTypeDeleteDialogComponent,
    EmployeeDialogComponent,
    EmployeeDeleteDialogComponent,
    MenuDialogComponent,
    MenuDeleteDialogComponent
  ],
  providers: [
    ProductTypeService,
    EmployeeTypeService,
    ProductService,
    EmployeeService,
    MenuService,
    LoginService,
    PatronGuard,
    CuisinierGuard,
    IsloggedInGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
