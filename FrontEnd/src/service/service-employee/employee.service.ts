import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {EmployeeType} from '../../model/EmployeeType';
import {Employee} from '../../model/Employee';

@Injectable()
export class EmployeeService {
  private linkApi = 'http://restauranthelha.azurewebsites.net/api/Employees';

  constructor(private http: HttpClient) {
  }

  public getAllEmployees(): Observable<any> {
    return this.http.get(this.linkApi);
  }

  public getEmployee() {

  }

  public createEmployee(employee: Employee): Observable<any> {
    console.log(JSON.stringify(employee.getCleanDataForSending()));
    return this.http.post(this.linkApi, employee.getCleanDataForSending(),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  public deleteEmployee(id: number): Observable<any> {
    return this.http.delete(this.linkApi, {
      params: new HttpParams().set('id', id + '')
    });
  }

  public updateEmployee(employee: Employee): Observable<any> {
    return this
      .http.put(this.linkApi, employee.getCleanDataForSending(), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        params: new HttpParams().set('id', employee.employeeID + '')
      });
  }

}
