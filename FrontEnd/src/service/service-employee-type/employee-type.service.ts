import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {EmployeeType} from '../../model/EmployeeType';

@Injectable()
export class EmployeeTypeService {

  constructor(private http: HttpClient) {
  }


  public getAllEmployeeTypes(): Observable<any> {
    return this.http.get('http://restauranthelha.azurewebsites.net/api/Employee_Type');
  }

  public createEmployeeType(employeeType: EmployeeType): Observable<any> {
    return this.http.post('http://restauranthelha.azurewebsites.net/api/Employee_Type', JSON.stringify(employeeType),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  public deleteEmployeeType(id: number): Observable<any> {
    return this.http.delete('http://restauranthelha.azurewebsites.net/api/Employee_Type', {
      params: new HttpParams().set('id', id + '')
    });
  }

  public updateEmployeeType() {

  }
}
