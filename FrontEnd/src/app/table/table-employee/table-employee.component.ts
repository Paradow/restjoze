import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {Employee} from '../../../model/Employee';
import {EmployeeDialogComponent} from '../../dialog/employee-dialog/employee-dialog.component';
import {EmployeeType} from '../../../model/EmployeeType';
import {EmployeeDeleteDialogComponent} from "../../dialog/employee-delete-dialog/employee-delete-dialog.component";

@Component({
  selector: 'app-table-employee',
  templateUrl: './table-employee.component.html',
  styleUrls: ['./table-employee.component.css']
})
export class TableEmployeeComponent implements OnInit {
  displayedColumns = ['lastNameEmployee', 'firstNameEmployee', 'mailAdress', 'viewUpdate'];

  @Input()
  private _employeeType: EmployeeType;

  private _dataSource: MatTableDataSource<Employee>;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;


  constructor(public dialog: MatDialog) {
  }


  ngOnInit() {
    this._dataSource = new MatTableDataSource(this._employeeType.employees);
  }

  ngAfterViewInit() {
    this._dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this._dataSource.filter = filterValue;
  }

  public openDialogEmployee(e: Employee) {
    const inputType = true;
    const dialogRef = this.dialog.open(EmployeeDialogComponent, {
        data: {
          employee: e,
          inputType: inputType,
          dataSource: this._dataSource
        }
      })
    ;
  }

  public openAddEmployeeDialog() {
    const e = new Employee('', '', '', '', this._employeeType.idEmployeeType , '', null, null, '', '', '');
    const inputType = false;
    const dialogRef = this.dialog.open(EmployeeDialogComponent, {
      data: {
        employee: e,
        inputType: inputType,
        employeeType: this.employeeType,
        dataSource : this.dataSource
      }
    });
  }
  public openDialogDelete(employee: Employee) {
    const index = this.employeeType.employees.findIndex(emp => emp.employeeID == employee.employeeID);
    const dialogRef = this.dialog.open(EmployeeDeleteDialogComponent, {
      data: {
        index: index,
        employees: this.employeeType.employees,
        dataSource: this._dataSource
      }
    });
  }

  public get dataSource(): MatTableDataSource<Employee> {
    return this._dataSource;
  }

  public set dataSource(value: MatTableDataSource<Employee>) {
    this._dataSource = value;
  }

  get employeeType(): EmployeeType {
    return this._employeeType;
  }

  set employeeType(value: EmployeeType) {
    this._employeeType = value;
  }
}
