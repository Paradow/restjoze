import { TestBed, async, inject } from '@angular/core/testing';

import { CuisinierGuard } from './cuisinier.guard';

describe('CuisinierGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuisinierGuard]
    });
  });

  it('should ...', inject([CuisinierGuard], (guard: CuisinierGuard) => {
    expect(guard).toBeTruthy();
  }));
});
