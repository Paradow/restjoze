export class Product {
  public idProd: number;
  public nameProd: string;
  public priceProd: number;
  public stockProd: number;
  public idProductType: number;
  public urlImage: string;

  constructor(productName: string, priceProduct: number, stockProduct: number, idProductType: number, urlImage: string) {
    this.nameProd = productName;
    this.priceProd = priceProduct;
    this.stockProd = stockProduct;
    this.idProductType = idProductType;
    this.urlImage = urlImage;
  }

  public static fromJson(rawProduct: any): Product {
    const idProd = rawProduct['IdProd'];
    const nameProd = rawProduct['NameProd'];
    const priceProd = rawProduct['PriceProd'];
    const stockProd = rawProduct['StockProd'];
    const idProductType = rawProduct['IdProductType'];
    const urlImage = rawProduct['urlImage'];

    const prod = new Product(nameProd, priceProd, stockProd, idProductType, urlImage);
    prod.idProd = idProd;

    return prod;
  }

  public static fromArrayJson(rawsProduct: any[]): Product[] {
    return rawsProduct.map(product => Product.fromJson(product));
  }

  public clone(): Product {
    const p = new Product(this.nameProd, this.priceProd, this.stockProd, this.idProductType, this.urlImage);
    p.idProd = this.idProd;
    return p;
  }
}
