import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {ProductTypeService} from "../../../service/service-product-type/product-type.service";
import {ProductService} from "../../../service/service-product/product.service";
import {Product} from "../../../model/Product";
import {ProductType} from '../../../model/ProductType';

@Component({
  selector: 'app-product-add-dialog',
  templateUrl: './product-add-dialog.component.html',
  styleUrls: ['./product-add-dialog.component.css']
})
export class ProductAddDialogComponent implements OnInit {
  private _product: Product = new Product("", 0, 0, 0, "");


  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public  productService: ProductService) {
  }

  ngOnInit() {
  }

  public onSubmit() {
    this.createProduct();
  }

  public createProduct() {
    this.product.idProductType = this.data.productType.productTypeID;
    this.productService.createProduct(this._product).subscribe(pfromdb => this.product.idProd = Product.fromJson(pfromdb).idProd);
    this.data.productType.product.push(this.product);
    this.data.dataSource._renderData.next(this.data.productType.product);
  }

  get product(): Product {
    return this._product;
  }

  set product(value: Product) {
    this._product = value;
  }
}
