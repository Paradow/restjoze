import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductType} from '../../model/ProductType';
import {ProductTypeService} from '../../service/service-product-type/product-type.service';
import {Menu} from '../../model/Menu';
import {Product} from '../../model/Product';

@Component({
  selector: 'app-add-product-menu',
  templateUrl: './add-product-menu.component.html',
  styleUrls: ['./add-product-menu.component.css']
})
export class AddProductMenuComponent implements OnInit {

  private _productTypes: ProductType[] = [];

  @Input()
  private _menu: Menu = new Menu('', 0, '', '', 0);

  @Output()
  private menuOut: EventEmitter<Menu> = new EventEmitter();

  constructor(public productTypeService: ProductTypeService) { }

  ngOnInit() {
    this.productTypeService.getAllProductType().subscribe(productTypes => this._productTypes = ProductType.fromArrayJson(productTypes));
  }

  public emitData(product: Product) {
    this._menu.product.push(product);
    this.menuOut.next(this._menu);
  }

  get productTypes(): ProductType[] {
    return this._productTypes;
  }

  set productTypes(value: ProductType[]) {
    this._productTypes = value;
  }

  getImage(p: Product) {
    return p.urlImage ? p.urlImage : 'http://www.emdocs.net/wp-content/themes/emdocs/images/default-image.jpg';
  }
}
