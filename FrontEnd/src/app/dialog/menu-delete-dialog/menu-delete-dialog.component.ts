import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {MenuService} from "../../../service/service-menu/menu.service";

@Component({
  selector: 'app-menu-delete-dialog',
  templateUrl: './menu-delete-dialog.component.html',
  styleUrls: ['./menu-delete-dialog.component.css']
})
export class MenuDeleteDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public menuService: MenuService) {
  }

  ngOnInit() {
  }

  public deleteMenu() {
    this.menuService.deleteMenu(this.data.menus[this.data.index].idMenu)
      .subscribe(
        response => {
          this.data.menus.splice(this.data.index, 1)
        },
        err => console.error(err)
      );
  }
}
