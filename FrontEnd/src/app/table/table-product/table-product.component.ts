import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Product } from '../../../model/Product';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ProductDialogComponent } from '../../dialog/product-dialog/product-dialog.component';
import { ProductDeleteDialogComponent } from '../../dialog/product-delete-dialog/product-delete-dialog.component';
import { ProductAddDialogComponent } from '../../dialog/product-add-dialog/product-add-dialog.component';
import { ProductTypeDeleteDialogComponent } from '../../dialog/product-type-delete-dialog/product-type-delete-dialog.component';
import { ProductType } from '../../../model/ProductType';
import { DataSource } from '@angular/cdk/collections';


@Component({
  selector: 'app-table-product',
  templateUrl: './table-product.component.html',
  styleUrls: ['./table-product.component.css']
})
export class TableProductComponent implements OnInit {
  displayedColumns = ['nameProduct', 'price', 'stock', 'viewUpdate'];

  @Input()
  private _productType: ProductType;

  private _dataSource: MatTableDataSource<Product>;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;


  constructor(public dialog: MatDialog) {

  }

  public openDialogProduct(p: Product) {
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      data: {
        product: p,
        urlImage: p.urlImage ? p.urlImage : 'http://www.emdocs.net/wp-content/themes/emdocs/images/default-image.jpg',
      }
    })
      ;
  }

  public openDialogDelete(product: Product) {
    const index = this.productType.product.findIndex(prod => prod.idProd == product.idProd);
    const dialogRef = this.dialog.open(ProductDeleteDialogComponent, {
      data: {
        index: index,
        products: this.productType.product,
        dataSource: this._dataSource
      }
    });
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.productType.product);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  get productType(): ProductType {
    return this._productType;
  }

  set productType(value: ProductType) {
    this._productType = value;
  }

  get dataSource(): MatTableDataSource<Product> {
    return this._dataSource;
  }

  set dataSource(value: MatTableDataSource<Product>) {
    this._dataSource = value;
  }

  public openAddProductDialog() {
    const dialogRef = this.dialog.open(ProductAddDialogComponent, {
      data: {
        dataSource: this.dataSource,
        productType: this.productType
      }
    });
  }
}

