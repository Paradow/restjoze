import {Component, OnInit, Inject} from '@angular/core';
import {Menu} from "../../../model/Menu";
import {MAT_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'app-menu-dialog',
  templateUrl: './menu-dialog.component.html',
  styleUrls: ['./menu-dialog.component.css']
})
export class MenuDialogComponent implements OnInit {

  private _tmpMenu: Menu = new Menu('', 0, '', '', 0);

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {
    if (!this.data.operation) {
      this.tmpMenu = this.data.menu.clone();
    }
  }

  get tmpMenu(): Menu {
    return this._tmpMenu;
  }

  set tmpMenu(value: Menu) {
    this._tmpMenu = value;
  }
}
