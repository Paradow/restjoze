import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ProductType } from '../../model/ProductType';
import { Headers, Http } from '@angular/http'

@Injectable()
export class ProductTypeService {

  constructor(private http: HttpClient) {
  }

  public getAllProductType(): Observable<any> {
    return this.http.get('http://restauranthelha.azurewebsites.net/api/Product_Type');
  }

  public getProductType() {

  }

  public createProductType(productType: ProductType): Observable<any> {
    return this.http.post('http://restauranthelha.azurewebsites.net/api/Product_Type', productType.getCleanDataForSending(),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  public deleteProductType(id: number): Observable<any> {
    return this.http.delete('http://restauranthelha.azurewebsites.net/api/Product_Type', {
      params: new HttpParams().set('id', id + '')
    });
  }

  public updateProductType() {

  }
}
