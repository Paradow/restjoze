import { TestBed, async, inject } from '@angular/core/testing';

import { PatronGuard } from './patron.guard';

describe('PatronGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PatronGuard]
    });
  });

  it('should ...', inject([PatronGuard], (guard: PatronGuard) => {
    expect(guard).toBeTruthy();
  }));
});
