import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ProductTypeService } from '../../../service/service-product-type/product-type.service';

@Component({
  selector: 'app-product-type-delete-dialog',
  templateUrl: './product-type-delete-dialog.component.html',
  styleUrls: ['./product-type-delete-dialog.component.css']
})
export class ProductTypeDeleteDialogComponent implements OnInit {

  constructor( @Inject(MAT_DIALOG_DATA) public data: any, public productTypeService: ProductTypeService) {
  }

  ngOnInit() {
    console.log(this.data.productTypes[this.data.index].productTypeID);
  }

  public deleteProductType() {
    const response = this.productTypeService.deleteProductType(this.data.productTypes[this.data.index].productTypeID)
      .subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      response => response = this.data.productTypes.splice(this.data.index, 1),
      err => console.error(err)
      );
  }
}
