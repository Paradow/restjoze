import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class PatronGuard implements CanActivate {
  constructor(private router:Router,public snackBar:MatSnackBar){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(localStorage.getItem("currentUser")){
        var token = JSON.parse(localStorage.getItem("currentUser")).token;
        let jwtData = token.split('.')[1];
        let decodedJwtJsonData = window.atob(jwtData);
        let decodedJwtData = JSON.parse(decodedJwtJsonData);
        let role = decodedJwtData.role;
        if(role === "Patron"){
          return true;
        }else{
          //this.router.navigate(['/access-denied']);
          
          this.snackBar.open("You don't have the permissions to acces this page","Close",{
            duration: 2000,
          })
          return false;
          
        }

      }else{
        this.router.navigate(['/login']);
        this.snackBar.open("Please login first","Close",{
          duration: 2000,
        })

        return false;
      }
      
      
    
  }
}
