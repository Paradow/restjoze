SET IDENTITY_INSERT [dbo].[Employee] ON
INSERT INTO [dbo].[Employee] ([idEmployee], [lastNameEmployee], [firstNameEmployee], [phoneNumber], [mailAdress], [idEmployeeType], [street], [num], [postalCode], [city], [username], [password]) VALUES (11, N'Admin', N'Helha     ', N'0477808080', N'helha@helha.com', 1, N'Campus Mons', 0, 7000, N'Mons', N'admin', N'helha')
INSERT INTO [dbo].[Employee] ([idEmployee], [lastNameEmployee], [firstNameEmployee], [phoneNumber], [mailAdress], [idEmployeeType], [street], [num], [postalCode], [city], [username], [password]) VALUES (13, N'user', N'Helha     ', N'0477008800', N'Helha@Helha.com', 2, N'Campus Mons', 0, 542, N'Mons', N'user', N'helha')
SET IDENTITY_INSERT [dbo].[Employee] OFF
