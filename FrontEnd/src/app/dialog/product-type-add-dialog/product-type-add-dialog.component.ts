import {Component, Inject, OnInit} from '@angular/core';
import {ProductType} from "../../../model/ProductType";
import {MAT_DIALOG_DATA} from "@angular/material";
import {ProductTypeService} from "../../../service/service-product-type/product-type.service";

@Component({
  selector: 'app-product-type-add-dialog',
  templateUrl: './product-type-add-dialog.component.html',
  styleUrls: ['./product-type-add-dialog.component.css']
})
export class ProductTypeAddDialogComponent implements OnInit {

  nameProductType: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public  productTypeService: ProductTypeService) {
  }

  ngOnInit() {
  }

  public createTypeProduct() {
    let tp = new ProductType(this.nameProductType);
    this.productTypeService.createProductType(tp).subscribe(tpfromdb => tp.productTypeID = ProductType.fromJson(tpfromdb).productTypeID);
    this.data.products.push(tp);
  }

}
