import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class IsloggedInGuard implements CanActivate {
  constructor(private router:Router, private snackBar:MatSnackBar){}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      

      if(localStorage.getItem("currentUser")){

        var token = JSON.parse(localStorage.getItem("currentUser")).token;
        let jwtData = token.split('.')[1];
        let decodedJwtJsonData = window.atob(jwtData);
        let decodedJwtData = JSON.parse(decodedJwtJsonData);
        let role = decodedJwtData.role;
        let username = decodedJwtData.unique_name;

        this.snackBar.open("You are already logged in as : "+username,"Close",{
          duration: 2000,
          
        },)
        if(role === "Patron"){
          
          this.router.navigate(['/management/products']);
          
          return false;
          
        }else if(role ==="Cuisinier"){
          
          this.router.navigate(['/management/menu']);
          
          
          return false;
          
        }

        return false;

      }else{


        return true;

      }  
  }

  
  
}
