USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Product_Type] Script Date: 12/8/2017 15:48:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Product_Type] (
    [idProductType]   INT          IDENTITY (1, 1) NOT NULL,
    [nameProductType] VARCHAR (40) NOT NULL
);


