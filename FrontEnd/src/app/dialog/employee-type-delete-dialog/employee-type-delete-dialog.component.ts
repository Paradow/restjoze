import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import { EmployeeTypeService } from '../../../service/service-employee-type/employee-type.service';
import { EmployeeType } from '../../../model/EmployeeType';

@Component({
  selector: 'app-employee-type-delete-dialog',
  templateUrl: './employee-type-delete-dialog.component.html',
  styleUrls: ['./employee-type-delete-dialog.component.css']
})
export class EmployeeTypeDeleteDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public  employeeTypeService: EmployeeTypeService) {
  }
  ngOnInit() {
  }

  public deleteEmployeeType(){
    const response = this.employeeTypeService.deleteEmployeeType(this.data.employeeTypes[this.data.index].idEmployeeType)
      .subscribe(
        response => response = this.data.employeeTypes.splice(this.data.index, 1),
        err => console.error(err)
      );
  }

}
