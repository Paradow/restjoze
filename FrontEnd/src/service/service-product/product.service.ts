import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ProductType } from '../../model/ProductType';
import { Product } from '../../model/Product';

@Injectable()
export class ProductService {

  private linkApi = 'http://restauranthelha.azurewebsites.net/api/Products';

  constructor(private http: HttpClient) {
  }

  public getAllProduct(): Observable<any> {
    return this.http.get(this.linkApi);
  }

  public getProduct() {

  }

  public createProduct(product: Product): Observable<any> {
    return this.http.post(this.linkApi, JSON.stringify(product),
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  public deleteProduct(id: number): Observable<any> {
    return this.http.delete(this.linkApi, {
      params: new HttpParams().set('id', id + '')
    });
  }

  public updateProduct(product: Product): Observable<any> {
    return this
      .http.put(this.linkApi, JSON.stringify(product), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        params: new HttpParams().set('id', product.idProd + '')
      });
  }
}
