import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTypeDeleteDialogComponent } from './employee-type-delete-dialog.component';

describe('EmployeeTypeDeleteDialogComponent', () => {
  let component: EmployeeTypeDeleteDialogComponent;
  let fixture: ComponentFixture<EmployeeTypeDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeTypeDeleteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeTypeDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
