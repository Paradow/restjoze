USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Employee_Type] Script Date: 12/8/2017 15:43:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee_Type] (
    [idEmployeeType]   INT          IDENTITY (1, 1) NOT NULL,
    [nameEmployeeType] VARCHAR (80) NOT NULL
);


