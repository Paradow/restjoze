SET IDENTITY_INSERT [dbo].[Product_Type] ON
INSERT INTO [dbo].[Product_Type] ([idProductType], [nameProductType]) VALUES (2, N'Plat')
INSERT INTO [dbo].[Product_Type] ([idProductType], [nameProductType]) VALUES (3, N'Dessert')
INSERT INTO [dbo].[Product_Type] ([idProductType], [nameProductType]) VALUES (25, N'Friandise')
INSERT INTO [dbo].[Product_Type] ([idProductType], [nameProductType]) VALUES (35, N'Boisson')
INSERT INTO [dbo].[Product_Type] ([idProductType], [nameProductType]) VALUES (37, N'Chips')
SET IDENTITY_INSERT [dbo].[Product_Type] OFF
