USE [dbRestaurant]
GO

/****** Object: Table [dbo].[Command] Script Date: 12/8/2017 15:41:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Command] (
    [idCommand]   INT      IDENTITY (1, 1) NOT NULL,
    [isServed]    BIT      NOT NULL,
    [idEmployee]  INT      NOT NULL,
    [dateCommand] DATETIME NOT NULL,
    [idBill]      INT      NOT NULL
);


