import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ProductType} from '../../model/ProductType';
import {ProductTypeService} from '../../service/service-product-type/product-type.service';
import {ProductTypeAddDialogComponent} from '../dialog/product-type-add-dialog/product-type-add-dialog.component';
import {ProductTypeDeleteDialogComponent} from '../dialog/product-type-delete-dialog/product-type-delete-dialog.component';

@Component({
  selector: 'app-list-type-produit',
  templateUrl: './list-type-produit.component.html',
  styleUrls: ['./list-type-produit.component.css']
})
export class ListTypeProduitComponent implements OnInit {

  private _productTypes: ProductType[] = [];
  private _displayedColumns = ['nameProduct', 'price', 'stock'];

  constructor(public productTypeService: ProductTypeService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.productTypeService.getAllProductType().subscribe(productTypes => this._productTypes = ProductType.fromArrayJson(productTypes));
  }

  get productTypes(): ProductType[] {
    return this._productTypes;
  }

  set productTypes(value: ProductType[]) {
    this._productTypes = value;
  }

  public openAddDialog() {
    const dialogRef = this.dialog.open(ProductTypeAddDialogComponent, {
      data: {
        products: this.productTypes
      }
    });
  }

  public openDeleteDialog(index: number) {
    const dialogRef = this.dialog.open(ProductTypeDeleteDialogComponent, {
      data: {
        productTypes: this._productTypes,
        index : index
      }
    });
  }
}
